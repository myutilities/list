//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//Ultimate Portable LIST Routine
//Very simple linked list routine
//The inventor is unknown but I will use it any how
//Source code is maintained on GITORIOUS by "forthnutter"
//https://git.gitorious.org/utilities/state_machine_engine.git
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#include "list.h"

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//Description:	Add an Item to the list
//Arguemts:		Pointer to list stucture
//				Pointer to the list item to add to the list
//Returns:		Pointer to the head list structure
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
LIST_ITEM *List_AddItem(LIST_ITEM *head, LIST_ITEM *item)
{         
    //LIST_ITEM* last;
	
    if(head == 0)				//if head is zero make new
    {
		head = item;			//the item now becomes the head
		item->next = item;		//the next item is Item
		item->previous = item;	//the items previous is item
    }
    else
    {
		item->previous = head->previous;
		head->previous = item;
		item->next = item->previous->next;
		item->previous->next = item;

		//item->next = head;	    // let this new task point to the front
		//last = head->previous;	// determine last item
		//head->previous = item;	// let the first point to the last
		//last->next = item;	    // let the last point to newly created
		//item->previous = last;  // point to last	  
    }    
    return(head);
}            

             
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//Description:	Delete the item form the the list
//Arguments:	Pointer to the list of Items structure
//				Pointer to the Item to remove
//Returns:	the head list structure
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
LIST_ITEM *List_RemoveItem(LIST_ITEM *head, LIST_ITEM *item)
{
        
    if(item == head)	//If the same may mean we are deleting only ITEM
    {	
		if(item->next == item)	//is next item pointing to itself
		{
			if(item->next == item->previous)	//is the next and previous point to itself
			{
				head = 0;	    //empty list if this is only one item
				item->next = item;	    //next is gone
				item->previous = item; //previous is gone
			}
		}
		else
		{
			head = item->next;
		}
    }

    item->next->previous = item->previous;	    // left of the item to be removed    
    item->previous->next = item->next;		    // right of the item to be removed      
	return(head);
}

