//*****************************************************************************
//Very simple linked list header file list.c
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#ifndef _LIST_H_
#define _LIST_H_

typedef struct TLIST_ITEM
{
    struct TLIST_ITEM *next;
    struct TLIST_ITEM *previous;
    void *owner;
}LIST_ITEM;

extern LIST_ITEM *List_AddItem(LIST_ITEM *head, LIST_ITEM *item);
extern LIST_ITEM *List_RemoveItem(LIST_ITEM *head, LIST_ITEM *item);

#endif
